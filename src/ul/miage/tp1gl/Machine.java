package ul.miage.tp1gl;

import java.io.*;
import java.util.*;

public class Machine {

    /**
     * Liste des ingrédients disponibles dans la machine
     */
    private final HashMap<String, Integer> ingredients;

    /**
     * Liste des boissons
     */
    private final ArrayList<Boisson> boissons;

    private static final String FILENAME = "src/ressources/ingredientList.txt";
    
    private static final String CAFE = "Café";
    private static final String CHOCOLAT = "Chocolat";
    private static final String LAIT = "Lait";
    private static final String THE = "Thé";
    private static final String SUCRE = "Sucre";
    private static final String NOBOISSONS = "Aucune boisson n'est enregistrée.\n";

    public Machine() {
        this.ingredients = new HashMap<>();
        this.initiateIngredients();

        this.boissons = new ArrayList<>();
        this.boissons.add(new Boisson("kfe", 4, 5, 2, 3, 1));
    }

    /**
     * Lecture du fichier texte afin d'initialiser les quantités des ingrédients.
     */
    private void initiateIngredients() {
        this.getIngredients().put(CAFE, 0);
        this.getIngredients().put(THE, 0);
        this.getIngredients().put(CHOCOLAT, 0);
        this.getIngredients().put(LAIT, 0);
        this.getIngredients().put(SUCRE, 0);

        try (FileReader fr = new FileReader(FILENAME);
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine())!= null) {
                String[] split = line.split("=");
                this.getIngredients().replace(split[0], Integer.parseInt(split[1]));
            }
        } catch (Exception e) {
            System.out.println("""
                    Une erreur s'est produite lors de l'initialisation des ingrédients.
                    Tous les stocks sont vides.
                    """);
        }
    }

    /**
     * Lancement de la machine
     */
    public void lancement() {
        System.out.println("""
                Merci de choisir une option.
                \t[1] Acheter une boisson
                \t[2] Ajouter une boisson
                \t[3] Modifier la composition d'une boisson
                \t[4] Supprimer une boisson
                \t[5] Ajouter une quantité pour le stock d'un ingrédient
                \t[6] Vérifier le stock des ingrédients
                \t[7] Quitter
                """
        );
        Scanner sc = new Scanner(System.in);
        System.out.print("Choix : ");
        switch (sc.nextLine()) {
            case "1" -> {
                this.commander();
                this.saveIngredients();
            }
            case "2" -> this.ajouterBoisson(null);
            case "3" -> this.modifierBoisson();
            case "4" -> this.supprimerBoisson();
            case "5" -> {
                this.modifierQuantiteIngredient();
                this.saveIngredients();
            }
            case "6" -> this.afficherStocks();
            case "7" -> {
                System.out.println("Merci, au revoir !");
                System.exit(0);
            }
            default -> System.out.println("\n/!\\ CHOIX INVALIDE");
        }
        this.lancement();
    }

    private void saveIngredients() {
        try {
            PrintWriter printWriter = new PrintWriter(FILENAME);
            for (Map.Entry<String, Integer> ingredient : this.getIngredients().entrySet()) {
                printWriter.println(ingredient.getKey() + "=" + ingredient.getValue());
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            System.out.println("Un problème s'est produit lors de la sauvegarde du stock restant.");
        }
    }

    /**
     * Effectuer une commande
     */
    private void commander() {
        if (boissons.isEmpty()) {
            System.out.println(NOBOISSONS);
        } else {
            System.out.println("\nEffectuer une commande :");
            Scanner sc = new Scanner(System.in);
            System.out.print("Quel montant voulez vous insérer ? ");
            int montant = this.demanderArgent(sc);
            System.out.println("Liste des boissons :");
            Boisson b = findBoisson();
            if (b == null) {
                System.out.println("Cette boisson n'existe pas !");
                System.out.println("La machine vous rend " + montant + "€.");
                return;
            }
            if (montant < b.getPrix()) {
                System.out.println("Vous n'avez pas mis assez d'argent !\n");
            } else {
                int cafe = this.getIngredients().get(CAFE);
                int choco = this.getIngredients().get(CHOCOLAT);
                int lait = this.getIngredients().get(LAIT);
                int the = this.getIngredients().get(THE);
                System.out.print("Combien de sucre voulez-vous ? ");
                int sucre = demanderQuantiteIngredient(new Scanner(System.in), "sucre");
                if (cafe < b.getNbCafe() ||
                        choco < b.getNbChocolat() ||
                        lait < b.getNbLait() ||
                        the < b.getNbThe() ||
                        sucre > this.getIngredients().get(SUCRE)
                ) {
                    System.out.println("Il manque des ingrédients.");
                    System.out.println("La machine vous rend " + montant + "€.");
                } else {
                    this.getIngredients().replace(CAFE, cafe - b.getNbCafe());
                    this.getIngredients().replace(CHOCOLAT, choco - b.getNbChocolat());
                    this.getIngredients().replace(LAIT, cafe - b.getNbLait());
                    this.getIngredients().replace(THE, the - b.getNbThe());
                    this.getIngredients().replace(SUCRE,  this.getIngredients().get(SUCRE) - sucre);
                    System.out.println("Voici votre " + b.getNom() + " ! La machine vous rend " + (montant - b.getPrix()) + "€.\n");
                }
            }
        }
    }

    /**
     * Modifie la quantite d'un ingredient
     */
    private void modifierQuantiteIngredient() {
        System.out.println("\nAjouter des ingrédients.");
        this.afficherStocks();
        Scanner sc = new Scanner(System.in);
        System.out.print("Pour quel ingredient voulez vous ajouter du stock ? ");
        String ingredient = sc.nextLine();
        if (this.getIngredients().containsKey(ingredient)) {
            System.out.print("Combien voulez vous en rajouter ? ");
            int quantite = this.demanderQuantiteIngredient(sc, null);
            quantite += this.getIngredients().get(ingredient);
            if (quantite < 0) {
                System.out.println("Vous avez atteint la quantité maximale pour cet ingrédient.");
            }
            this.getIngredients().replace(ingredient, Math.abs(quantite));
            System.out.println("La quantité a bien été modifiée.\n");
        } else {
            System.out.println("Cet ingrédient n'existe pas.\n");
        }
    }

    /**
     * Modifier une boisson
     */
    private void modifierBoisson() {
        if (this.getBoissons().isEmpty()) {
            System.out.println(NOBOISSONS);
        } else {
            System.out.println("\nModifier une boisson : ");
            Boisson b = this.findBoisson();
            if (b != null) {
                if (!this.ajouterBoisson(b.getNom())) {
                    System.out.println("Votre boisson n'a pas été modifiée.");
                    return;
                }
                this.getBoissons().remove(b);
                System.out.println("Votre boisson a été modifiée.\n");
            } else {
                System.out.println("Cette boisson n'existe pas.\n");
            }
        }
    }

    private Boisson findBoisson() {
        for (Boisson b : this.getBoissons()) {
            System.out.println("\t\t - " + b.getNom());
        }
        Scanner sc = new Scanner(System.in);
        System.out.print("\tSur quelle boisson voulez vous effectuer cette action ? ");
        String nom = sc.nextLine();
        return this.getBoissonByNom(nom);
    }

    /**
     * Supprime une boisson
     */
    private void supprimerBoisson() {
        if (this.getBoissons().isEmpty()) {
            System.out.println(NOBOISSONS);
        } else {
            System.out.println("\nSupprimer une boisson : ");
            Boisson b = this.findBoisson();
            if (b != null) {
                this.getBoissons().remove(b);
                System.out.println("Votre boisson a été supprimée.\n");
            } else {
                System.out.println("Cette boisson n'existe pas.\n");
            }
        }
    }

    /**
     * Retourne une boisson en fonction de son nom
     *
     * @param nom nom
     * @return Boisson
     */
    private Boisson getBoissonByNom(String nom) {
        for (Boisson b : this.getBoissons()) {
            if (b.getNom().equalsIgnoreCase(nom)) {
                return b;
            }
        }
        return null;
    }

    /**
     * Ajoute une boisson
     *
     * @return boolean
     */
    private boolean ajouterBoisson(String nomEnregistre) {
        System.out.println("\n Ajouter une boisson: ");
        if (this.getBoissons().size() < 5) {
            Scanner sc = new Scanner(System.in);
            // nom
            String nom = "";
            if (nomEnregistre == null) {
                System.out.print("\tNom de la boisson : ");
                nom = this.demanderNomBoisson(sc);
            } else {
                nom = nomEnregistre;
            }
            // prix
            System.out.print("\tPrix de la boisson : ");
            int prix = this.demanderArgent(sc);
            // nb cafe
            System.out.print("\tNombre de café : ");
            int nbCafe = this.demanderQuantiteIngredient(sc, null);
            // nb lait
            System.out.print("\tNombre de lait : ");
            int nbLait = this.demanderQuantiteIngredient(sc, null);
            // nb chocolat
            System.out.print("\tNombre de chocolat : ");
            int nbChoco = this.demanderQuantiteIngredient(sc, null);
            // nb the
            System.out.print("\tNombre de thé : ");
            int nbThe = this.demanderQuantiteIngredient(sc, null);

            if (nbCafe == 0 && nbChoco == 0 && nbLait == 0 && nbThe == 0) {
                System.out.println("Une boisson ne peut pas etre vide !\n");
                return false;
            }  else {
                this.getBoissons().add(new Boisson(nom, prix, nbCafe, nbLait, nbChoco, nbThe));
            }
            System.out.println("Votre boisson a bien été ajoutée.\n");
        } else {
            System.out.println("Vous ne pouvez pas ajouter plus de 3 boissons.\n");
        }
        return true;
    }


    /**
     * Demande la quantite d'un ingredient
     *
     * @param sc scanner
     * @param sucre String
     * @return prix
     */
    private int demanderQuantiteIngredient(Scanner sc, String sucre) {
        int quantite;
        String prixS = sc.nextLine();
        try {
            quantite = Integer.parseInt(prixS);
        } catch (Exception e) {
            System.out.print("Merci de rentrer un nombre entier : ");
            quantite = this.demanderQuantiteIngredient(sc, null);
        }
        if (quantite < 0) {
            System.out.print("Merci de rentrer une quantité supérieure à 0 : ");
            return this.demanderQuantiteIngredient(sc, null);
        }
        if (sucre != null) {
            if (quantite > 5) {
                System.out.print("La quantité de sucre doit etre entre 0 et 5 :");
                return this.demanderQuantiteIngredient(sc, sucre);
            }
        }
        return quantite;
    }

    /**
     * Demander une quantite d'argent
     *
     * @param sc scanner
     * @return prix
     */
    private int demanderArgent(Scanner sc) {
        int prix;
        String prixS = sc.nextLine();
        try {
            prix = Integer.parseInt(prixS);
        } catch (Exception e) {
            System.out.print("Merci de rentrer un nombre entier : ");
            prix = this.demanderArgent(sc);
        }
        if (prix <= 0) {
            System.out.print("Merci de rentrer un prix supérieur à 0 : ");
            return this.demanderArgent(sc);
        }
        return prix;
    }

    /**
     * Demande le nom d'une boisson
     *
     * @param sc scanner
     * @return nom
     */
    private String demanderNomBoisson(Scanner sc) {
        String nom = sc.nextLine();
        while (this.boissonExiste(nom) || nom.equalsIgnoreCase("")) {
            System.out.print("\tCette boisson existe déj ou est invalide. Veuillez choisir un autre nom : ");
            nom = sc.nextLine();
        }
        return nom;
    }

    /**
     * Vérifie si une boisson existe déjà
     *
     * @param nom nom de la boisson
     * @return existance
     */
    private boolean boissonExiste(String nom) {
        for (Boisson b : this.getBoissons()) {
            if (b.getNom().equalsIgnoreCase(nom)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Affiche le stock de tous les ingrédients
     */
    private void afficherStocks() {
        System.out.println("\n Liste des ingrédients restants: ");
        for (Map.Entry<String, Integer> ingredient : this.getIngredients().entrySet()) {
            System.out.println("\t - " + ingredient.getKey() + " : " + ingredient.getValue());
        }
        System.out.println("\n");
    }

    /**
     * Getter ingredients
     *
     * @return liste des ingredients
     */
    public Map<String, Integer> getIngredients() {
        return ingredients;
    }

    public List<Boisson> getBoissons() {
        return boissons;
    }
}
